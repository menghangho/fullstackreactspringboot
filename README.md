# full stack react project

## Issues
* about annotation
  * @PrePersist
  * @PreUpdate
  * @NotBlank
  * @Size
  * @JsonFormat
  * binding result
  * @ControllerAdvice
  * cascade?
* bootstrap
  * https://getbootstrap.com/docs/4.0/components/navbar/
  * 
## initial
```
git init


```
## add README.md
```
git add .
git commit
```
## goto [starter](spring.start.io)
### code Backend
### add `devtools`,`jpa`,`web`,`mysql`,`h2`

### branch
```
git branch
git branch branch0
git branch
git checkout branch0
```

### add server side project initial
#### unzip files
#### git add .
#### try to build
```
mvnw install
```


#### add class Project
```

```

#### add project lombok
#### enable auto import
```
	<dependency>
		<groupId>org.projectlombok</groupId>
		<artifactId>lombok</artifactId>
		<version>1.18.12</version>
		<scope>provided</scope>
	</dependency>
```
#### add @Data in front of Project

#### Add projectRepository
* extends CrudRepository
* reference this [JPA/CRUD](https://stackoverflow.com/questions/14014086/what-is-difference-between-crudrepository-and-jparepository-interfaces-in-spring)

#### Add ProjectService.java
* connect project service to the repository

#### Add ProjectController.java
* use ProjectService to interact

#### in ARC, post, endpoint 'api/'
```
{
  "projectName":"Test project1",
  "projectIdentifier":"ID_Test1",
  "description":"a new project"
}
```

#### check ARC, check db-console

#### remote
```
git checkout master
git merge branch0
git remote add origin https://menghangho@bitbucket.org/menghangho/fullstackreactspringboot.git
git push -u origin master
```

#### branch1
```
git branch branch1
git checkout branch1
```

#### add constraint
#### add Valid
#### add BindingResult
```
if (bindingResult.hasErrors()) {
            return new ResponseEntity<>("Invalid Project Object",HttpStatus.BAD_REQUEST);
        }
```
### change signature
```
public ResponseEntity<?> createNewProject
```

### change to List
```
return new ResponseEntity<List<FieldError>>(bindingResult.getFieldErrors(), HttpStatus.BAD_REQUEST);
```

### change to Map
```
           Map<String, String> errorMap = new HashMap<>();
            for (FieldError error : bindingResult.getFieldErrors()) {
                errorMap.put(error.getField(), error.getDefaultMessage());
            }
            return new ResponseEntity<Map<String, String>>(errorMap, HttpStatus.BAD_REQUEST);
```

### use this to test, get reasonable result
```
{
 

}
```

### branch2
```
git branch branch2
git checkout branch2
```

#### move message extraction from main body

#### 處理重複的500 error

* @Valid 代表 project物件是合理的project
* @Column(unique=true) ==>要放進去db之後才會知道, 所以是已經通過@Valid

#### 實作自訂的handling
* 增加一個runtime exception的子類別
* 增加ExceptionResponse
* 增加CustomResponseEntityExceptionHandler
  * @Controller
  * @ControllerAdvice
* 修改ProjectService實作try catch
  * 丟出ProjectIdException

#### 增加查詢
* findByProjectIdentifier
* 如果Project找不到一樣丟出Exception

#### 增加全部找到
* 在service layer中直接呼叫findAll
* 在Repository中出現list

#### 刪除Projct
* 增加ProjectService中刪除的函數
* 增加ProjectController中刪除的內容

### branch5

* front end

#### IDE plugins
* Babel ES6/ES7 (by Dzannotti)
* Bracket Pair Colorizer(by CoenraadS)
* ES7 React Redux GraphQL(by dsznajder)
* Javascript(ES6) code snippets(by charalampos)
* Live Server(by Ritwick Dey)
Node.JS module Intelligense(by Zongmin Lei)
Path Intellisense (by Christian Kohler)
Prettier - Code Formatter(by Esben Petersen)

#### create app
```
npx create-react-app frontend
```

##### start app
* npm start

#### Add component
* a component need to be contained into 1 div

#### install
* npm install bootstrap
* add bootstrap in App.js
```
import "bootstrap/dist/css/bootstrap.min.css"
```
* then using the style with `className`

### add font awesome
```
 <script src="https://kit.fontawesome.com/628a92eb38.js" crossorigin="anonymous"></script>
```

### npm install react-router-dom
* route and route exact

### 只有沒有onChange會變成唯讀

### onChange可以在每一個field去bind

### Redux
* 透過axios跟spring boot
* install redux
  * npm i redux react-redux redux-thunk
  * npm i axios
* store.js ==> redux store

## make actions folder

## for server, add crossorigin annotation for that controller

## install classNames
```
npm i classnames
```
## 增加型別
## 增加成錯誤的樣式

## add URL filtering
```
export const getProject = (id, history) => async dispatch => {
  try {
    const res = await axios.get(`http://localhost:8080/api/project/${id}`);
    dispatch({
      type: GET_PROJECT,
      payload: res.data
    });
  } catch (error) {history.push("/dashboard")}
};

```

### after modify package.json, restart server

### Add insert for backlog


## Move data to postgresql

### Install postgreSQL
* postgres/password 5443

### Pgadmin4
* connect, create database full_stack_react

### add exception handling

### remove cascade

### React again

#### add types

### Finish type definition