---
marp: true
theme: patristar
header: "Spring Boot & React"
footer: "mark@patristar.com"
---

<style>
section {
  font-family: 'Noto Sans CJK TC',  sans-serif;
}
</style>

# Spring Boot & React

# Full Stack Development

---

<!--
paginate: true
-->

---

# start.spring.io &

# project setup

---

## start.spring.io

### Gradle, Jar

#### Description: Spring Boot Backend

#### Package: Jar

#### Dependency:

- devtools, spring-boot-starter-jpa, web, h2, lombok

---

## start.spring.io

### sample configuration

![](images/2020-07-19-16-39-36.png)

---

## start.spring.io

### sample configuration list

- (https://start.spring.io/#!type=gradle-project&language=java&platformVersion=2.3.1.RELEASE&packaging=jar&jvmVersion=1.8&groupId=com.patristar.demo&artifactId=BootBackend&name=BootBackend&description=spring%20Boot%20Backend&packageName=com.patristar.demo.BootBackend&dependencies=devtools,data-jpa,web,h2,lombok)

---

## Project Structure:

### BootBackend

- Using IntelliJ
- File/New/Project from existing sources...
  - Import project from external model
    - choose Gradle

### React Frontend

- Using Visual Studio Code

### Git control at this level

- `git init`
- `git add README.md`
- `git commit -m "..."`

---

# 實作專案類別的永續化

## 產生 Bean 物件

## 生成 repository

## 生成 service

## 生成 Rest API

---

# JPA Entity 的生命周期

- `@PrePersist`
  - 一個新物件要被寫入 DB 之前
- `@PostPersist`
  - 一個新物件被寫入之後
- `@PreRemove`
  - 一個物件被移除之前
- `@PostRemove`
  - 一個物件被移除之後
- `@PreUpdate`
  - 一個物件被更新之前
- `@PostUpdate`
  - 一個物件被更新之後
- `@PostLoad`
  - 一個物件被載入之後

---

# Add Project Domain Object

## 建立在`model`子目錄下

```java
package com.patristar.demo.BootBackend.model;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.persistence.*;
import java.util.Date;
@Entity
@Data
@NoArgsConstructor
public class Project {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String projectName;
    private String projectIdentifier;
    private String description;
    private Date startDate;
    private Date endDate;
```

---

# Add Project Domain Object

## 增加`@PrePersist`與`@PreUpdate`

```java
    private Date createdAt;
    private Date updatedAt;
    @PrePersist
    protected void onCreate() {
        this.createdAt = new Date();
    }

    @PreUpdate
    protected void onUpdate() {
        this.updatedAt = new Date();
    }
}

```

---

# 增加 repository

## 在`repositories/`子目錄下增加`ProjectRepository`介面

```java
package com.patristar.demo.BootBackend.repositories;

import com.patristar.demo.BootBackend.model.Project;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProjectRepository extends
        CrudRepository<Project, Long> {

}
```

---

# 增加 service

## 在`services\`子目錄下增加`ProjectService`類別, 使用`ProjectRepository`, 並且實作`saveOrUpdateProject`

```java
@Service
public class ProjectService {
    @Autowired
    private ProjectRepository projectRepository;

    public Project saveOrUpdateProject(Project project) {
        return projectRepository.save(project);

    }
}
```

---

# ResponseEntity

## `ResponseEntity` 表示整個 HTTP 的回應, 包含狀態碼, 標題和內文

- 可以組態完整的 HTTP 回應

## 在 endpoint 傳回值, spring 就會處理之後的

- 第二個參數可以是`HttpStatus.OK`, `HttpStatus.BAD_REQUEST`
- 也可以在第二個參數傳入 HTTP 檔頭

---

# 增加 RestController

## 在`controllers`目錄下增加`ProjectController`類別

## 使用`ProjectService`新增資料

```java
@RestController
@RequestMapping("/api/project")
public class ProjectController {
    @Autowired
    ProjectService projectService;
    @PostMapping("")
    public ResponseEntity<Project> createNewProject(@RequestBody Project project) {
        Project project1 = projectService.saveOrUpdateProject(project);
        return new ResponseEntity<>(project1, HttpStatus.CREATED);

    }
}
```

---

# 使用 Advanced Rest Client

## 使用`POST`, URL 設定`http://localhost:8080/api/project`用 json 傳入一個 project 物件

![](images/2020-07-19-23-44-49.png)

---

# 使用 Advanced Rest Client

## 傳回值是 201

## 可以看到 createdAt 的時間

```json
{
  "id": 1,
  "projectName": "React with Spring Boot",
  "projectIdentifier": "boot_react1",
  "description": "boot project test1",
  "startDate": null,
  "endDate": null,
  "createdAt": "2020-07-19T15:40:13.842+00:00",
  "updatedAt": null
}
```

---

# 修改 JSon 的格式

---

# JSON 格式在 bean 裡的設定

## 使用`@JsonFormat`, 是一種 Jackson 的表示法顯示如何輸出

## 通常指定如何輸出`Date`和`Calendar`的值

## 通常作用於`SimpleDateFormat`物件

## 定義於`jackson-databind`套件

## 使用`pattern=yyyy-MM-dd@HH:mm:ss.SSSZ`之類的方式設定

---

# 在 bean 中的`Date`欄位套用`@JsonFormat`, 大小寫要注意 yyyy-**MM**-dd

```
@Entity
@Data
@NoArgsConstructor
public class Project {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String projectName;
    private String projectIdentifier;
    private String description;
    @JsonFormat(pattern = "yyyy-MM-dd") //add1
    private Date startDate;
    @JsonFormat(pattern = "yyyy-MM-dd") //add2
    private Date endDate;
    @JsonFormat(pattern = "yyyy-MM-dd") //add3
    private Date createdAt;
    @JsonFormat(pattern = "yyyy-MM-dd") //add4
    private Date updatedAt;
```

---

# 重新送出 post 之後得到的結果, createdAt 的格式變變成`yyyy-MM-dd`

```
{
"id": 1,
"projectName": "React with Spring Boot",
"projectIdentifier": "boot_react1",
"description": "boot project test1",
"startDate": null,
"endDate": null,
"createdAt": "2020-07-21",
"updatedAt": null
}
```

---

# Constraints & Validation

---

# SpringBoot 的 Validation

## 使用`@NotBlank`到某個成員變數, 並且提供`message`

## 當 spring 驗証這個 bean 時會依照限制去執行

## 在 RestController 的函數參數中加上`@Valid`, 會自動執行 JSR380 的實作, 也就是 hibernate validator 去驗証參數

## 如果參數出錯, 會丟出`MethodArgumentNotValidException`

## 可以傳入`BindingResult`來測試或者是檢驗錯誤

---

# 在 ProjectController 中加入`BindingResult`錯誤判定

## 如果 binding 有錯,就產生 BAD_REQUEST, 所以要修改傳回型別成 ResponseEntity<?>
## 在下一頁引入import之後要加上`@Valid @RequestBody`

```
    @PostMapping("")
    public ResponseEntity<?> createNewProject(@RequestBody Project project, //ResponseEntity的型態改為?
                                             BindingResult bindingResult) { //增加binding result
        if (bindingResult.hasErrors()) { //增加錯誤的判定
            return new ResponseEntity<>("Invalid Project Object", HttpStatus.BAD_REQUEST);
        }
        Project project1 = projectService.saveOrUpdateProject(project);
        return new ResponseEntity<>(project1, HttpStatus.CREATED);

    }
```

---

# `build.gradle`增加 validation

```
dependencies {
	implementation 'org.springframework.boot:spring-boot-starter-data-jpa'
	implementation 'org.springframework.boot:spring-boot-starter-web'
	implementation 'org.springframework.boot:spring-boot-starter-validation' // add this
	compileOnly 'org.projectlombok:lombok'
	developmentOnly 'org.springframework.boot:spring-boot-devtools'
	runtimeOnly 'com.h2database:h2'
	annotationProcessor 'org.projectlombok:lombok'
	testImplementation('org.springframework.boot:spring-boot-starter-test') {
		exclude group: 'org.junit.vintage', module: 'junit-vintage-engine'
	}
}
```

---

# Error Handling

---

# 增加 constraints

```java
public class Project {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @NotBlank(message = "project name is required") //1
    private String projectName;
    @NotBlank(message="Project Identifier is required") //2
    @Size(min=6,max=16, message="Identifier should between 6 to 16 characters") //3
    @Column(updatable = false, unique = true) //4
    private String projectIdentifier;
    @NotBlank(message = "description is required") //5
    private String description;
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date startDate;
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date endDate;
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date createdAt;
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date updatedAt;

```

---

# 執行完成後, 此時 Rest 如果呼叫時傳入不夠的參數就會觸發例外, 並且帶出 trace

```

"timestamp": "2020-07-22T01:49:12.252+00:00",
"status": 500,
"error": "Internal Server Error",
"trace": "org.springframework.transaction.TransactionSystemException:
 Could not commit JPA transaction;
 nested exception is javax.persistence.RollbackException:
 Error while committing the transaction at
 org.springframework.orm.jpa.JpaTransactionManager.doCommit(
```

---

# 在 server spring boot log 出現的錯誤訊息

```
javax.validation.ConstraintViolationException:
Validation failed for classes [com.patristar.demo.BootBackend.model.Project]
during persist time for groups [javax.validation.groups.Default, ]
List of constraint violations:[
	ConstraintViolationImpl
  {interpolatedMessage='description is required',
  propertyPath=description, rootBeanClass=class com.patristar.demo.BootBackend.model.Project,
  messageTemplate='description is required'}

	ConstraintViolationImpl{interpolatedMessage='Project Identifier is required',
  propertyPath=projectIdentifier,
  rootBeanClass=class com.patristar.demo.BootBackend.model.Project,
  messageTemplate='Project Identifier is required'}
]
```

---

# 增加 validation

## 在`@ResponseBody`中加入`@Valid`, 此時的 bind 會被趨動

```
    @PostMapping("")
    public ResponseEntity<?> createNewProject(@Valid @RequestBody Project project,
                                             BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return new ResponseEntity<>("Invalid Project Object",
            HttpStatus.BAD_REQUEST);
        }
        Project project1 = projectService.saveOrUpdateProject(project);
        return new ResponseEntity<>(project1, HttpStatus.CREATED);

    }
```

---

# 檢視 Rest 的 Output, 它會是 400 Internal Server Error

## 錯誤訊息就是`Invalid Project Object`

## 此時的 server 就不會有 exception, 因為在`@Valid`時就傳回了

## 但是此時的物件是寫死的, 都是`Invalid Project Object`

---

# 修改輸出, 直接從 fieldError 中取值並顯示在畫面上

## `new ResponseEntity<>(bindingResult.getFieldErrors(),HttpStatus.BAD_REQUEST)`

```
   @PostMapping("")
    public ResponseEntity<?> createNewProject(@RequestBody Project project,
                                              BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return new ResponseEntity<>(bindingResult.getFieldErrors(),
                    HttpStatus.BAD_REQUEST);

        }
        Project project1 = projectService.saveOrUpdateProject(project);
        return new ResponseEntity<>(project1, HttpStatus.CREATED);

    }
```

---

# 此時檢查 Rest 傳回值, 就可以查到錯誤是一個 json 型態, 裡面只要有違反限制的就會顯示回應在 json 的內容中

```

  {
"codes": [
  "NotBlank.project.description",
  "NotBlank.description",
  "NotBlank.java.lang.String",
  "NotBlank"
],
"arguments": [
  {...}
],
"defaultMessage": "description is required",
"objectName": "project",
"field": "description",
"rejectedValue": null,
"bindingFailure": false,
"code": "NotBlank"
},
```

---

# 簡化 JSON 的輸出

## 使用一個 Map, 只收集`Field`和`DefaultMessage`的內容

```
   @PostMapping("")
    public ResponseEntity<?> createNewProject(@Valid @RequestBody Project project,
                                              BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            // 如果有錯, 集合錯誤到errorMap傳回給呼叫端
            Map<String, String> errorMap = new HashMap<>();
            for (FieldError error : bindingResult.getFieldErrors()) {
                errorMap.put(error.getField(), error.getDefaultMessage());
            }
            return new ResponseEntity<>(errorMap, HttpStatus.BAD_REQUEST);

        }
        Project project1 = projectService.saveOrUpdateProject(project);
        return new ResponseEntity<>(project1, HttpStatus.CREATED);

    }
```

---

# 檢視 Rest 服務得到的回應

## 此時如果有錯誤訊息, 會集合到一個 dict 字典中, 鍵值是 bean 中的元件名, 屬性定義於限制中

## 如果有多筆資料就會依處理程序排列

```
{
"description": "description is required",
"projectIdentifier": "Project Identifier is required"
}
```

---

# 將處理錯誤訊息的類別抽離出來

## 產生`MapValidationError`

## 產生`MapValidation`

## 實作`MapValidation`

## 在`RestController`中呼叫

---

# 重構錯誤解析類別, 產生`MapValidationError`在 service 下

```java
public class MapValidationError {
    public static ResponseEntity<?> MapValidation(BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            // 如果有錯, 集合錯誤到errorMap傳回給呼叫端
            Map<String, String> errorMap = new HashMap<>();
            for (FieldError error : bindingResult.getFieldErrors()) {
                errorMap.put(error.getField(), error.getDefaultMessage());
            }
            return new ResponseEntity<>(errorMap, HttpStatus.BAD_REQUEST);

        }
        return null;
    }
}

```

---

# 在`ProjectController`中的`createNewProject`中呼叫

## 如果 errorMap 不為空就回傳

## 用 Rest 測試結果不應該改變

```java
   @PostMapping("")
    public ResponseEntity<?> createNewProject(@Valid @RequestBody Project project,
                                              BindingResult bindingResult) {
        // 替換成抽出後的類別
        ResponseEntity<?> errorMap = MapValidationError.MapValidation(bindingResult);
        if (errorMap != null) {
            return errorMap;
        }
        Project project1 = projectService.saveOrUpdateProject(project);
        return new ResponseEntity<>(project1, HttpStatus.CREATED);

    }
```

---

# Spring Boot Rest 的例外處理

---

# 重複給定 ProjectIdentifier 會有錯誤發生

## 使用 POST

## URL `http://localhost:8080/api/project`

## Body Content Type `application/json`

```json
{
  "projectName": "React with Spring Boot",
  "description": "app1",
  "projectIdentifier": "APPTEST1"
}
```

---

# 重複執行兩次的錯誤是 500 Bad Request

## 在 server side 的錯誤訊息是`Unique index or primary key violation:`

## 為了讓前端便於處理需要有更好的方式

```Json
{
"timestamp": "2020-07-23T16:46:40.006+00:00",
"status": 500,
"error": "Internal Server Error",
"trace": "org.springframework.dao.DataIntegrityV..."
```

---

# Exception Handling

## `@ResponseStatus`可以設定 Http 的狀態

- 一般是 HTTP 200

## 如果要指定 controll 函數的回應狀態, 可以使用`@ResponseStatus`

## 如果要指定錯誤狀態, 可以用

- `@ResponseStatus(HttpStatus.BAD_REQUEST)`

## 錯誤的處理

- 使用`@ExceptionHandler`
- 使用`@ControlerAdvice`

---

# 將例外定義成`@ResponseStatus`

## 最直觀的方式是直接將例外標記`ResponseStatus`

```java
@ResponseStatus(code = HttpStatus.BAD_REQUEST)
class CustomException extends RuntimeException {}

```

## 當 Spring 攔結到`Exception`, 會使用`@ResponseStatus`的設定

## Spring 一定會呼叫`HttpServletResponse.sendError()`

---

# Rest 的錯誤處理

## Spring3.2 前是 HandlerExceptionResolver 或者是 @ExceptionHandler

## Spring3.2 之後是@ControllerAdvice 提供對整個應用程式一致的例外處理

## 使用全域的`@ExceptionHandler`搭配`@ControllerAdvice`

---

# `@ControllerAdvice`的使用方式

### 將散在各地的`@ExceptionHandlers`集中在一個單一全域錯誤處理元件

### 完整控制回應的本體和狀態碼

### 將多個例外對應到同一個函數, 可以一起處理

### 可以搭配`@ResponseEntity`

---

# 在`exceptions`目錄下產生`ProjectIdException`是`RuntimeException`的子類別

## 注意加上`@ResponseStatus`

```Java
@ResponseStatus(HttpStatus.BAD_REQUEST)
public class ProjectIdException extends RuntimeException {
    public ProjectIdException(String message) {
        super(message);
    }
}
```

---

# 產生`ProjectIdExceptionResponse`類別

## 使用`@Data`與`@AllArgsConstructor`

```Java
@Data
@AllArgsConstructor
public class ProjectIdExceptionResponse {
    private String projectIdentifier;
}

```

---

# 產生`CustomResponseEntityExceptionHandler`

## 父類別是`ResponseEntityExceptionHandler`

## 加上@ControllerAdvice

```Java

@ControllerAdvice
public class CustomResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {
    @ExceptionHandler
    public final ResponseEntity<Object> handleProjectIdException
    (ProjectIdException exception, WebRequest request){
        ProjectIdExceptionResponse response = new ProjectIdExceptionResponse(exception.getMessage());
        return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);

    }
}
```

---

# 修改`ProjectService`, 增加例外處理

## 如果儲存出錯就觸發`ProjectIdException`

```Java
@Service
public class ProjectService {
    @Autowired
    private ProjectRepository projectRepository;

    public Project saveOrUpdateProject(Project project) {
        try {
            project.setProjectIdentifier(project.getProjectIdentifier().toUpperCase());
            return projectRepository.save(project);
        } catch (Exception e) {
            throw new ProjectIdException("Project ID" +
                    project.getProjectIdentifier().toUpperCase() + " already exists");

        }

    }
}
```

---

# 使用同樣的 ProjectIdentifier 會得到錯誤訊息

## 注意狀態會是 400

```
{
"projectIdentifier": "Project IDAPPTEST1 already exists"
}
```

## server 上的錯誤是

```
Unique index or primary key violation:
 "PUBLIC.UK_NH7JG4QYW1DM5Y0BN2VWOQ6V2_INDEX_1 ON PUBLIC.PROJECT(PROJECT_IDENTIFIER)
 VALUES 1"; SQL statement:
insert into project (created_at, description, end_date, project_identifier, project_name, start_date, updated_at, id)
values (?, ?, ?, ?, ?, ?, ?, ?) [23505-200]
```

---

# Retrieve all project

---

# 在`ProjectService`中增加`findAllProjects()`

```Java
@Service
public class ProjectService {
    @Autowired
    private ProjectRepository projectRepository;

    public Project saveOrUpdateProject(Project project) {
      // as origin
    }

    public Iterable<Project> findAllProjects() {
        return projectRepository.findAll();
    }

}
```

---

# 在`ProjectController`中增加`/all`的端點, 傳回所有的專案

```Java
@RestController
@RequestMapping("/api/project")
public class ProjectController {
    @Autowired
    ProjectService projectService;

    @PostMapping("")
    public ResponseEntity<?> createNewProject(@Valid @RequestBody Project project,
                                              BindingResult bindingResult) {
        // as origin
    }
    @GetMapping("/all")
    public Iterable<Project> getAllProjects() {
        return projectService.findAllProjects();
    }
}
```

---

# 使用`GET`於`http://localhost:8080/api/project/all`看到的結果

```
{
"id": 1,
"projectName": "React with Spring Boot",
"projectIdentifier": "APP_SAMPLE1",
"description": "app1",
"startDate": null,
"endDate": null,
"createdAt": "2020-07-24",
"updatedAt": null
}
```

---

# 使用專案 id 取得專案

---

# `ProjectRepository`中加入`findByProjectIdentifier`函數

## Spring Boot 會自動 生成內容

```java
@Repository
public interface ProjectRepository extends
        CrudRepository<Project, Long> {
    Project findByProjectIdentifier(String projectId);
}
```

---

# 在`ProjectService`中增加`findProjectById`函數, 找不到產生 Exception

```
@Service
public class ProjectService {
    @Autowired
    private ProjectRepository projectRepository;
    public Project saveOrUpdateProject(Project project) {
        //no change
    }

    public Iterable<Project> findAllProjects() {
        // no change
    }

    public Project findProjectByIdentifier(String projectId) {
        Project project = projectRepository.findByProjectIdentifier(projectId.toUpperCase());
        if (project == null) {
            throw new ProjectIdException("Project ID" + projectId + " does not exist");
        }
        return project;
    }
}
```

---

# 在`ProjectController`中增加`{/projectId}`名稱為 `getProjectById(@PathVariable String projectId)`的函數

```
@RestController
@RequestMapping("/api/project")
public class ProjectController {
    @Autowired
    ProjectService projectService;

    @PostMapping("")
    public ResponseEntity<?> createNewProject(@Valid @RequestBody Project project,
                                              BindingResult bindingResult) {
    // no change
    }

    @GetMapping("/all")
    public Iterable<Project> getAllProjects() {
        return projectService.findAllProjects();
    }

    @GetMapping("/{projectId}")
    public ResponseEntity<?> getProjectById(@PathVariable String projectId) {
        Project project = projectService.findProjectByIdentifier(projectId);
        return new ResponseEntity<>(project, HttpStatus.OK);

    }
}

```

---

#使用`GET`並且傳入`projectId`(是字串)例如`http://localhost:8080/api/project/APP_SUPER1`

```JSon
{
"id": 1,
"projectName": "React with Spring Boot",
"projectIdentifier": "APP_SUPER1",
"description": "app1",
"startDate": null,
"endDate": null,
"createdAt": "2020-07-24",
"updatedAt": null
}
```

---

# Delete Project

---

# 在`ProjectService.java`中實作`deleteProjectById`

```
    public void deleteProjectByIdentifier(String projectId) {
        // hand writing
        Project project = projectRepository.
        findByProjectIdentifier(projectId.toUpperCase());

        if (project == null) {
            throw new ProjectIdException("Cannot delete project with id: '"
            +projectId+"', this project does not exist");
        }
        projectRepository.delete(project);

    }
```

---

# 在`ProjectController.java`中實作`@DeleteMapping`的`deleteProjectByProjectId`函數

```java
@RestController
@RequestMapping("/api/project")
public class ProjectController {
    @Autowired
    ProjectService projectService;
    @Autowired
    private MapValidationErrorService mapValidationErrorService;
    @PostMapping("")
    public ResponseEntity<?> createNewProject(@Valid @RequestBody Project project,
     BindingResult bindingResult) {
      //same
    }
    @GetMapping("/{projectId}")
    public ResponseEntity<?> getProjectById(@PathVariable String projectId ) {
      //same
    }
    @GetMapping("/all")
    public Iterable<Project> getAllProjects() {
        return projectService.findAllProjects();
    }
    @DeleteMapping("/{projectId}")
    public ResponseEntity<?> deleteProjectByProjectId(@PathVariable  String projectId) {
        projectService.deleteProjectByIdentifier(projectId);
        return new ResponseEntity<>("Project with ID:"+projectId+" was deleted",HttpStatus.OK);
    }
}

```

---

# 使用`DELETE`, `http://localhost:8080/api/project/all/APP_SUPER1`, 最後接自己的專案 ID(`project_identifier`), 顯示的

```
Project with ID:APP_SUPER1 was deleted
```

---

# 和 React 的整合

---

# CORS

## Cross Origin Resource Sharing (CORS)

    * HTML5和JS的客戶端透過rest API存取資料

## 透常 JS 所在的機器和 API 所在的不會在同一台機器

## cors 可以啟動跨網域的存取

## 只要啟動`@CrossOrigin`即可

---

# `@CrossOrigin`使用的時機

## 跟`@RequestMapping`一起使用

### 預設是所有的來源都行

### 允許的 http 行為是`@RequestMapping`中定義的

### CORS preflight request 是一個 CORS request 去檢查 cors 是否被伺服器所支援, 可以用 maxAge 來設定時間

---

# 在`ProjectController`上面設定 CrossOrigin

```
@RestController
@CrossOrigin // add this
@RequestMapping("/api/project")
public class ProjectController {
    @Autowired
    ProjectService projectService;


}

```

---

# 實作專案到待辦事項

---

# 使用`@JsonIgnore`

## 是 Jackson 函式庫中的方式, 可以選擇物件到 JSON 時不要輸出

## 可以在類別層面寫`@JsonIgnoreProperty(value={"變數名"})`

## 對欄位用`@JsonIgnore`

## 如果用`@JsonIgnoreType`來取消整個類別的輸出

## 也可以創建一個`@JsonFilter` 再去作細節的控制

---

# @OneToOne 跟 @JoinColumn

---

# 產生 Backlog 的類別, 指向 Project

```Java
@Entity
@Data
@NoArgsConstructor
public class Backlog {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private Integer PTSequence = 0;
    private String projectIdentifier;
    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "project_id",nullable = false)
    @JsonIgnore
    private Project project;

}
```

---

# 帶`Project中指定反過來的對應`

```Java
@Entity
@Data
@NoArgsConstructor
public class Project {
    // ...
    @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL, mappedBy = "project")
    private Backlog backlog;
    // ...
}

```

---

# 增加 BacklogRepository

```Java
@Repository
public interface BacklogRepository extends CrudRepository<Backlog, Long> {
    Backlog findByProjectIdentifier(String identifier);
}
```

---

# 修改`ProjectService`中的`saveOrUpdateProject`函數, 增加`backlog`

```
    public Project saveOrUpdateProject(Project project) {
        String projectIdentifier = project.getProjectIdentifier().toUpperCase();
        try {
            project.setProjectIdentifier(projectIdentifier);
            if (project.getId() == null) { // 如果是新專案就創建
                Backlog backlog = new Backlog();
                project.setBacklog(backlog);
                backlog.setProject(project);
                backlog.setProjectIdentifier(projectIdentifier);

            }
            if (project.getId() != null) { // 如果是舊專案就更新
                project.setBacklog(
                    backlogRepository.findByProjectIdentifier(projectIdentifier));
            }
            return projectRepository.save(project);
        } catch (Exception e) {
            throw new ProjectIdException("Project ID" + projectIdentifier + " already exists");
        }
    }
```

---

# 使用 Rest 的客戶端新增一筆資料測試

## 使用 POST

## URL 使用`http://localhost:8080/api/project`

## Body Content Type 使用`application/json`

```Java
{
  "projectName":"React with Spring Boot",
  "description":"app1",
  "projectIdentifier":"APP_SUPER1"
}
```

---

# 檢視結果

## 在 spring boot 的 log 中找到 h2 資料庫的 URL

```
H2 console available at '/h2-console'. Database available at 'jdbc:h2:mem:0feb44cb-0802-44f8-9308-a0a6a40c167e'
```

## 登入到 h2 console 中`http://localhost:8080/h2-console/`

### 填入 URL

### 帳號`sa`密碼空白

## 之後可以看到有 BACKLOG 的表格

### 查詢之後注意 PROJECT_IDENTIFIER 是所選定的 project

---

# 修改 h2-database 到較易使用的 api

## 修改`application.properties`

## 之後登入有固定的 url 可以使用, 並且用`sa`,`sa`登入

```yaml
spring.datasource.url=jdbc:h2:mem:react;DB_CLOSE_DELAY=-1;DB_CLOSE_ON_EXIT=FALSE
spring.datasource.driverClassName=org.h2.Driver
spring.datasource.username=sa
spring.datasource.password=sa
spring.jpa.database-platform=org.hibernate.dialect.H2Dialect
```

---

# 實作待辦事項到待辦項目

---

# 新增`ProjectTask.java`

```Java
@Entity
@Data
@NoArgsConstructor
public class ProjectTask {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(updatable = false)
    private String projectSequence;
    @NotBlank(message = "please include a task summary")
    private String summary;
    private String acceptanceCriteria;
    private String status;
    private Integer priority;
    private Date dueDate;
    private Date createAt;
    private Date updateAt;
```

---

# 新增`ProjectTask.java`

```Java
    @Column(updatable = false)
    private String projectIdentifier;
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "backlog_id", updatable = false, nullable = false)
    @JsonIgnore
    private Backlog backlog;

    @PrePersist
    public void onCreate() {
        this.createAt = new Date();
    }

    @PreUpdate
    public void onUpdate() {
        this.updateAt = new Date();
    }
}
```

---

# 在 Backlog 中加入 ProjectTask 的對映, 為 1 對多的關係

```
@Entity
@Data
@NoArgsConstructor
public class Backlog {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private Integer PTSequence = 0;
    private String projectIdentifier;
    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "project_id", nullable = false)
    @JsonIgnore
    private Project project;

    // 一個Backlog中會有多筆ProjectTask
    @OneToMany(cascade = CascadeType.REFRESH,
            fetch = FetchType.EAGER, mappedBy = "backlog")
    private List<ProjectTask> projectTasks = new ArrayList<>();
}
```

---

# 增加`ProjectTaskRepository`介面, 延伸`CrudRepository`另外加上@Repository

```Java
package com.patristar.demo.BootBackend.repositories;

import com.patristar.demo.BootBackend.model.ProjectTask;
import org.springframework.data.repository.CrudRepository;
@Repository
public interface ProjectTaskRepository extends
        CrudRepository<ProjectTask, Long> {
}

```

---

# 增加`ProjectTaskService` part1

```Java
@Service
public class ProjectTaskService {
    @Autowired
    private BacklogRepository backlogRepository;
    @Autowired
    private ProjectTaskRepository projectTaskRepository;

    public enum Priority {
        NOT_SET(0), LOW(1), MEDIUM(2), HIGH(3);
        public final int value;

        private Priority(int value) {
            this.value = value;
        }
    }
    public enum Status {
        NOT_SET(""),
        TO_DO("TO_DO");
        public final String value;

        private Status(String value) {
            this.value = value;
        }
    }

```

---

# 增加`ProjectTaskService` part2

```Java
    public ProjectTask addProjectTask(String projectIdentifier,
                                      ProjectTask projectTask) {
        Backlog backlog = backlogRepository.
                findByProjectIdentifier(projectIdentifier);
        projectTask.setBacklog(backlog);
        // 每次遞增序列
        Integer backlogSequence = backlog.getPTSequence();
        backlogSequence++;
        backlog.setPTSequence(backlogSequence);
        // 設定ProjectSequence
        projectTask.setProjectSequence(projectIdentifier + "-" + backlogSequence);
        projectTask.setProjectIdentifier(projectIdentifier);
        // 設定重要性
        if (projectTask.getPriority() == null ||
        projectTask.getPriority() == Priority.NOT_SET.value) {
            projectTask.setPriority(Priority.MEDIUM.value);
        }
        if (projectTask.getStatus() == null ||
        projectTask.getStatus().equals(Status.NOT_SET.value)) {
            projectTask.setStatus(Status.TO_DO.value);
        }
        return projectTaskRepository.save(projectTask);
    }

}

```

---

# 增加`BacklogController`

```Java
@RestController
@RequestMapping("/api/backlog")
@CrossOrigin
public class BacklogController {
    @Autowired
    private ProjectTaskService projectTaskService;

    @PostMapping("/{project_id}")
    public ResponseEntity<?> addTaskToBacklog(@Valid @RequestBody ProjectTask projectTask,
                                              BindingResult result,
                                              @PathVariable String project_id) {
        ResponseEntity<?> errorMap = MapValidationError.MapValidation(result);
        if (errorMap != null) {
            return errorMap;
        }
        ProjectTask projectTaskToReturn =
                projectTaskService.addProjectTask(project_id, projectTask);
        return new ResponseEntity<>(projectTaskToReturn, HttpStatus.CREATED);
    }

}

```

---

# Happy Path 的加入資料

## 假設 project id 為`APP_SUPER1`

## 使用 POST, url 設定為`http://localhost:8080/api/backlog/APP_SUPER1`

## BODY 中的 application/json 設定為

```Json
{
  "summary":"project item1"
}
```

---

# 資料加入之後的傳回值

```Json
{
"id": 2,
"projectSequence": "APP_SUPER1-2",
"summary": "project item2",
"acceptanceCriteria": null,
"status": "TO_DO",
"priority": 2,
"dueDate": null,
"createAt": "2020-07-27T10:48:33.730+00:00",
"updateAt": null,
"projectIdentifier": "APP_SUPER1"
}
```

---

## 錯誤的輸入

### 假設 project id 為`APP_SUPER1`

### 使用 POST, url 設定為`http://localhost:8080/api/backlog/APP_SUPER1`

### BODY 中的 application/json 設定為

```Json
{
  "acceptance_criteria":"get the thing done"
}
```

### 檢視到的結果會是

```
{
"summary": "please include a task summary"
}
```

---

# 取得專案工作內容

---

# 在`ProjectTaskRepository`時依照 id 取得專案工作項目

```Java
public interface ProjectTaskRepository extends
        CrudRepository<ProjectTask, Long> {
    List<ProjectTask> findByProjectIdentifierOrderByPriority(String id);
}

```

---

# 在 ProjectTaskService 中增加`findTasksById`

```
@Service
public class ProjectTaskService {
    @Autowired
    private BacklogRepository backlogRepository;
    @Autowired
    private ProjectTaskRepository projectTaskRepository;
    // ...
    //...
    public Iterable<ProjectTask> findTasksById(String backlog_id) {
        return projectTaskRepository.
                findByProjectIdentifierOrderByPriority(backlog_id);
    }
}

```

---

# 在 BacklogController 中加入取得某專案工作項目的端點

```Java
@RestController
@RequestMapping("/api/backlog")
@CrossOrigin
public class BacklogController {
    @Autowired
    private ProjectTaskService projectTaskService;
    // ...
    @GetMapping("/{project_id}")
    public Iterable<ProjectTask> getProjectTasks(@PathVariable String project_id) {
        return projectTaskService.findTasksById(project_id);
    }

}
```

---

## 使用 REST 取得專案列表

### 使用 GET, url 是`http://localhost:8080/api/backlog/APP_SUPER1` 取得的結果是

```json
[
  {...},
  {...},
  {
"id": 3,
"projectSequence": "APP_SUPER1-3",
"summary": "add something new3",
"acceptanceCriteria": null,
"status": "TO_DO",
"priority": 2,
"dueDate": null,
"createAt": "2020-07-27T16:06:44.056+00:00",
"updateAt": null,
"projectIdentifier": "APP_SUPER1"
}
],
```

---

# 新增專案項目 project_id 產生錯誤時的例外處理

---

## 在新增專案項目時如果碰到不對的 project_id 就會有 500 的 internal error

### 使用 POST, url 是`http://localhost:8080/api/backlog/APP_SUPER123`

## body 照舊

```
{
  "summary":"add something new"
}
```

### 會產生 exception(500 internal error)

```
"timestamp": "2020-07-27T16:21:56.276+00:00",
"status": 500,
"error": "Internal Server Error",
```

---

## 產生`ProjectIdIncorrectException`, 並且加上`@ResponseStatus`

```
@ResponseStatus(HttpStatus.BAD_REQUEST)
public class ProjectIdIncorrectException
        extends RuntimeException {
    public ProjectIdIncorrectException(String message) {
        super(message);
    }
}

```

---

## 產生`ProjectIdIncorrectExceptionResponse`

```Java
package com.patristar.demo.BootBackend.exceptions;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ProjectIdIncorrectExceptionResponse {
    private String projectIdIncorrect;
}

```

---

## 在`CustomResponseEntityExceptionHandler`中增加`handleProjectIdIncorrectException`

```Java
@ControllerAdvice
public class CustomResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {
    //... the same...

    @ExceptionHandler
    public final ResponseEntity<Object>
    handleProjectIdIncorrectException(ProjectIdIncorrectException exception,
                                      WebRequest request) {
        ProjectIdExceptionResponse response =
                new ProjectIdExceptionResponse(exception.getMessage());
        return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);

    }
}
```

---

## 在 ProjectTastService 中檢查 project_id 是否正確

### 在 ProjectTaskService 中曾加 ProjectRepository 的 field 並且增加@Autowired

```Java
@Service
public class ProjectTaskService {
    @Autowired
    private BacklogRepository backlogRepository;
    @Autowired
    private ProjectTaskRepository projectTaskRepository;
    @Autowired
    private ProjectRepository projectRepository;

```

---

## 在`ProjectTaskService`中的`addProjectTask`中檢查 backlog, 如果取回是 null 就丟出 ProjectIdIncorrectException

```Java
@Service
public class ProjectTaskService {
    //...
    public ProjectTask addProjectTask(String projectIdentifier,
                                      ProjectTask projectTask) {
        Backlog backlog = backlogRepository.
                findByProjectIdentifier(projectIdentifier);
        // check projectIdentifier is real point to a project
        if (backlog == null) {
            throw new ProjectIdIncorrectException(
                    String.format("project not found by id:%s",projectIdentifier));
        }
    //...
```

---

## 在`ProjectTaskService`中的`findTasksById`中檢查`project_id`是否合法, 如果不是就丟出 ProjectIdIncorrectException

```Java
@Service
public class ProjectTaskService {
    //...
    public Iterable<ProjectTask> findTasksById(String backlog_id) {
        Project project = projectRepository.findByProjectIdentifier(backlog_id);
        if (project == null) {
            throw new ProjectIdIncorrectException(
                    String.format("Project with id:%s does not exist",backlog_id));
        }
    //...
```

---

## 使用不正確的 project_id 增加專案項目

### 使用 POST, url 是`http://localhost:8080/api/backlog/APP_SUPER1234`, 後面的 project_id 不存在

### 給定合適的 summary

```json
{
  "summary": "add something new"
}
```

### 得到的結果

```
{
"projectIdentifier": "project not found by id"
}
```

---

## 使用不正確的 id 條列項目

### 使用 GET, url 是`http://localhost:8080/api/backlog/APP_SUPER1234`, 後面的 project_id 不存在

### 執行後的結果是

```
{
"projectIdentifier": "Project with id:APP_SUPER1234 does not exist"
}
```

---

# 找到單一的專案工作項目

---

## 在`ProjectTaskRepository`中增加`findByProjectSequence`

```Java
public interface ProjectTaskRepository extends
        CrudRepository<ProjectTask, Long> {
    List<ProjectTask> findByProjectIdentifierOrderByPriority(String id);
    // add this
    ProjectTask findByProjectSequence(String sequence);
}

```

---

## 在 ProjectTask 將欄位增加 unique

```Java
@Entity
@Data
@NoArgsConstructor
public class ProjectTask {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    // add this
    @Column(updatable = false, unique = true)
```

---

## 帶`ProjectTaskService`中增加

```Java
@Service
public class ProjectTaskService {
    @Autowired
    private BacklogRepository backlogRepository;
    @Autowired
    private ProjectTaskRepository projectTaskRepository;
    @Autowired
    private ProjectRepository projectRepository;
    //...
    // add this
    public ProjectTask findTaskByProjectSequence(String task_id) {
        return projectTaskRepository.findByProjectSequence(task_id);
    }

```

---

## 建立`TaskController`, 端點是`/api/task`

### 加上`getProjectTask`函數傳入路徑中的 task_id

```Java
@RestController
@RequestMapping("/api/task")
@CrossOrigin
public class TaskController {
    @Autowired
    private ProjectTaskService projectTaskService;

    @GetMapping("/{task_id}")
    public ResponseEntity<?> getProjectTask(@PathVariable String task_id){
        ProjectTask projectTask = projectTaskService.findTaskByProjectSequence(task_id);
        return new ResponseEntity<>(projectTask, HttpStatus.OK);
    }
}

```

---

## 測試時先建立專案, 再新增專案項目

### 取得 id 例如是`APP_SUPER1-1`, 此處 id 不能打錯

### 之後使用 GET, url 是`http://localhost:8080/api/task/APP_SUPER1-1`

```
200 OK
16.83 ms

{
"id": 1,
"projectSequence": "APP_SUPER1-1",
"summary": "add something new",
"acceptanceCriteria": null,
"status": "TO_DO",
"priority": 2,
"dueDate": null,
"createAt": "2020-07-28T02:34:16.793+00:00",
"updateAt": null,
"projectIdentifier": "APP_SUPER1"
}
```

---

# Task id (project_sequence)不存在時的處理

---

## 產生 ProjectNotFoundException, 是一個 runtime exception

```Java
@ResponseStatus(HttpStatus.BAD_REQUEST)
public class TaskNotFoundException extends RuntimeException {
    public TaskNotFoundException(String message) {
        super(message);
    }
}
```

---

## 產生 TaskNotFoundExceptionResponse, 儲存例外時的 task_id

```Java
@Data
@AllArgsConstructor
public class TaskNotFoundExceptionResponse {
    private String task_id;
}

```

---

## 增加`handleTaskNotFoundException`的處理於 CustomResponseEntityExceptionHandler 下

### 注意傳入的是 TaskNotFoundException

```
@ControllerAdvice
public class CustomResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {
    //...
        @ExceptionHandler
    public final ResponseEntity<Object>
    handleTaskNotFoundException(TaskNotFoundException exception,
                                      WebRequest request) {
        TaskNotFoundExceptionResponse response =
                new TaskNotFoundExceptionResponse(exception.getMessage());
        return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);

    }

}


```

---

## 在 ProjectTaskService 中加入例外的處理, 如果無法取得 task 就丟出例外

```
@Service
public class ProjectTaskService {
    @Autowired
    private BacklogRepository backlogRepository;
    @Autowired
    private ProjectTaskRepository projectTaskRepository;
    @Autowired
    private ProjectRepository projectRepository;
    //...
       public ProjectTask findTaskByProjectSequence(String task_id) {
        ProjectTask task = projectTaskRepository.findByProjectSequence(task_id);
        // chck the valid of task_id
        if (task == null) {
            throw new TaskNotFoundException("task with ID:" + task_id + " does not exist ");
        }
        return task;
    }
}

```

---

## 在 Rest 上使用不存在的 id 作測試

### 使用 GET, url 設定成`http://localhost:8080/api/task/APP_SUPER1-1x`

```
{
"task_id": "task with ID:APP_SUPER1-1x does not exist "
}
```

---

# 專案項目的更新

---

## ProjectTask 中取消 backlog 的 updatable

```Java
@Entity
@Data
@NoArgsConstructor
public class ProjectTask {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    //...
    // update JoinColumn
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "backlog_id",  nullable = false)
    @JsonIgnore
    private Backlog backlog;

    //...
}

```

---

## ProjectTaskService 中加入 updateByProjectSequence

## 取得 backlog, 連結之後再儲存

```Java
@Service
public class ProjectTaskService {
    @Autowired
    private BacklogRepository backlogRepository;
    @Autowired
    private ProjectTaskRepository projectTaskRepository;
    @Autowired
    private ProjectRepository projectRepository;
    public ProjectTask updateByProjectSequence(ProjectTask updateTask){
        Backlog backlog = backlogRepository.
                findByProjectIdentifier(updateTask.getProjectIdentifier());
        updateTask.setBacklog(backlog);
        return projectTaskRepository.save(updateTask);
    }
}
```

---

## 在 TaskController 上新增 patch 端點, 輸入 JSon 更新

```Java
@RestController
@RequestMapping("/api/task")
@CrossOrigin
public class TaskController {
    @Autowired
    private ProjectTaskService projectTaskService;

    @PatchMapping("")
    public ResponseEntity<?> updateProjectTask(@Valid @RequestBody ProjectTask projectTask,
                                               BindingResult result) {
        ResponseEntity<?> errorMap = MapValidationError.MapValidation(result);
        if (errorMap != null) {
            return errorMap;
        }
        ProjectTask updatedTask = projectTaskService.updateByProjectSequence(projectTask);
        return new ResponseEntity<>(updatedTask, HttpStatus.OK);
    }
}

```

---

## 測試時需要新增專案, 新增專案項目

### 使用該新增後傳回的值作更新, 使用 PATCH, url=`http://localhost:8080/api/task`

### 注意其它的欄位不要刪除, 這樣原有的屬性才能夠保留

```
{
"id": 1,
"projectSequence": "APP_SUPER1-1",
"summary": "update to something new",
"acceptanceCriteria": "I want to add something",
"status": "TO_DO",
"priority": 2,
"dueDate": null,
"createAt": "2020-07-28T04:43:10.342+00:00",
"updateAt": null,
"projectIdentifier": "APP_SUPER1"
}
```

---

## 作完之後用原本的方式查詢

### 使用 GET, url=`http://localhost:8080/api/task/APP_SUPER1-1`

### 注意 update 的時間有被更新

```json
{
  "id": 1,
  "projectSequence": "APP_SUPER1-1",
  "summary": "update to something new",
  "acceptanceCriteria": "I want to add something",
  "status": "TO_DO",
  "priority": 2,
  "dueDate": null,
  "createAt": "2020-07-28T04:43:10.342+00:00",
  "updateAt": "2020-07-28T04:43:58.211+00:00",
  "projectIdentifier": "APP_SUPER1"
}
```

---

# 資料的刪除

---

## 在 ProjectTaskService 下實作`deleteByProjectSequence`

```Java
@Service
public class ProjectTaskService {
    @Autowired
    private BacklogRepository backlogRepository;
    @Autowired
    private ProjectTaskRepository projectTaskRepository;
    @Autowired
    private ProjectRepository projectRepository;
    // ...
    public void deleteTaskByProjectSequence(String task_id) {
        ProjectTask task = findTaskByProjectSequence(task_id);
        projectTaskRepository.delete(task);
    }


}
```

---

## 在 TaskController 下實作刪除的站點

```Java
@RestController
@RequestMapping("/api/task")
@CrossOrigin
public class TaskController {
    @Autowired
    private ProjectTaskService projectTaskService;
    // ...
    @DeleteMapping("/{task_id}")
    public ResponseEntity<?>
    deleteProjectTask(@PathVariable String task_id){
        projectTaskService.deleteTaskByProjectSequence(task_id);
        return new ResponseEntity<>("Project task:"
        +task_id+" was deleted successfully", HttpStatus.OK);
    }

}
```

---

## 在 Rest 下實作刪除

### 使用 DELETE, url 是`http://localhost:8080/api/task/APP_SUPER1-1`

### 得到的結果是

```
Project task:APP_SUPER1-1 was deleted successfully
```

---

## 更新專案的刪除

### 如果刪除專案要把 1 對多的項目刪除

```Java
@Entity
@Data
@NoArgsConstructor
public class Backlog {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    //...
    // 一個Backlog中會有多筆ProjectTask
    // 增加CascadeType.REMOVE
    // 增加orphanRemovl=true
    @OneToMany(cascade = CascadeType.REMOVE,orphanRemoval = true,
            fetch = FetchType.EAGER, mappedBy = "backlog")
    private List<ProjectTask> projectTasks = new ArrayList<>();


}

```

---

# Spring Boot 的測試

## 都是建立在 src/test 目錄下

## Spring 允許使用各種層級的測試

---

## 測試 Repository

```Java
@DataJpaTest
public class ProjectRepositoryTest {
    @Autowired
    private ProjectRepository projectRepository;
    @Test
    public void testAdd() {
        Project project = new Project();
        project.setProjectName("project1");
        project.setProjectIdentifier("TEST_PROJECT1");
        project.setDescription("Hi this is my project description");
        projectRepository.save(project);
        assertEquals(projectRepository.count(),1);
    }
}

```

---

## 測試 Service(part1)

```Java
@SpringBootTest
public class ProjectServiceTest {
    @Autowired
    private ProjectService projectService;

    @BeforeEach
    public void testPreCondition() {
        assertNotNull(projectService);
    }
```

---

## 測試 Service(part2)

```
    @Test
    public void testAdd() {
        Project project1 = new Project();
        project1.setProjectName("project1");
        project1.setProjectIdentifier("TEST_PROJECT1");
        project1.setDescription("Hi this is my project description");

        Project project2 = new Project();
        project2.setProjectName("project2");
        project2.setProjectIdentifier("TEST_PROJECT2");
        project2.setDescription("Hi this is my project description");

        projectService.saveOrUpdateProject(project1);
        projectService.saveOrUpdateProject(project2);
        for (Project project : projectService.findAllProjects()) {
            System.out.println(project.getProjectName());
        }
    }
}
```

---

## 測試 ProjectRestController (part1)

```Java
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ProjectRestControllerTest {
    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    public void greetingShouldReturnDefaultMessage() throws Exception {
        Project project = new Project();
        project.setProjectName("project1");
        project.setProjectIdentifier("TEST_PROJECT1");
        project.setDescription("Hi this is my project description");

```

---

## 測試 ProjectRestController (part2)

```Java
        restTemplate.postForObject("http://localhost:" + port + "/api/project",
                project, Project.class);
        ResponseEntity<Project[]> responseEntity =
                restTemplate.getForEntity("http://localhost:" + port + "/api/project/all",
                        Project[].class);
        Project[] projects = responseEntity.getBody();
        MediaType contentType = responseEntity.getHeaders().getContentType();
        HttpStatus statusCode = responseEntity.getStatusCode();
        assertEquals(projects.length, 1);
        assertThat(contentType).isEqualByComparingTo(MediaType.APPLICATION_JSON);
        assertThat(statusCode).isEqualByComparingTo(HttpStatus.OK);
    }
}
```

---

# 切換到 MSSQL

---

## 使用 docker

```shell
sudo docker run -e "ACCEPT_EULA=Y" -e "SA_PASSWORD=uCoM1@34" \
   -p 1433:1433 --name mssql \
   -d mcr.microsoft.com/mssql/server:2019-latest
docker ps -a
docker exec -it mssql "bash"
/opt/mssql-tools/bin/sqlcmd -S localhost -U SA -P uCoM1@34
```

## 創建表格

```shell
CREATE DATABASE ReactBoot
SELECT Name from sys.Databases
GO
```

---

## 在 gradle 中加入 driver

## https://mvnrepository.com/artifact/com.microsoft.sqlserver/mssql-jdbc/8.2.0.jre8

```
dependencies {
	implementation 'org.springframework.boot:spring-boot-starter-data-jpa'
	implementation 'org.springframework.boot:spring-boot-starter-web'
	implementation 'org.springframework.boot:spring-boot-starter-validation'
	compileOnly 'org.projectlombok:lombok'
	developmentOnly 'org.springframework.boot:spring-boot-devtools'
	runtimeOnly 'com.h2database:h2'
	annotationProcessor 'org.projectlombok:lombok'
	testImplementation('org.springframework.boot:spring-boot-starter-test') {
		exclude group: 'org.junit.vintage', module: 'junit-vintage-engine'
	}
	// https://mvnrepository.com/artifact/com.microsoft.sqlserver/mssql-jdbc
	compile group: 'com.microsoft.sqlserver', name: 'mssql-jdbc', version: '8.2.0.jre8'
}

```

---

## 將原本的 h2 的`application.properties`改成`application-h2.properties`

```
spring.datasource.url=jdbc:h2:mem:react;DB_CLOSE_DELAY=-1;DB_CLOSE_ON_EXIT=FALSE
spring.datasource.driverClassName=org.h2.Driver
spring.datasource.username=sa
spring.datasource.password=sa
spring.jpa.database-platform=org.hibernate.dialect.H2Dialect
```

---

## 新增 application-mssql.properties

```
spring.h2.console.enabled=false
spring.datasource.url=jdbc:sqlserver://127.0.0.1;databaseName=ReactBoot
spring.datasource.username=SA
spring.datasource.password=uCoM1@34
spring.datasource.driverClassName=com.microsoft.sqlserver.jdbc.SQLServerDriver
spring.jpa.show-sql=true
spring.jpa.properties.hibernate.format_sql = true
spring.jpa.hibernate.dialect=org.hibernate.dialect.SQLServer2012Dialect
spring.jpa.hibernate.ddl-auto = create
```

---

## 在 IntelliJ 的 Launcher, Main class 下方有一個 VM Options

### 加入`-Dspring.profiles.active=h2`即可以用 h2 啟動

### 加入`-Dspring.profiles.active=h2`即可以用 mssql 啟動
---
## 使用cache層
---
### 在`build.gradle`中加入內容

```groovy
	implementation 'org.ehcache:ehcache'
	implementation 'javax.cache:cache-api'
```
### 在/src/main/resources中加入`ehcache.xml`
```
<config xmlns='http://www.ehcache.org/v3'>
    <cache alias="projects">
        <key-type>org.springframework.cache.interceptor.SimpleKey</key-type>
        <value-type>java.util.List</value-type>
        <expiry>
            <ttl unit="minutes">5</ttl>
        </expiry>
        <resources>
            <heap unit="kB">200</heap>
        </resources>
    </cache>
</config>
```
---
### 在application.properties中對應
```
spring.cache.jcache.config=classpath:ehcache.xml
```
### 主程式中enable cache使用`EnableCaching`
```java
@EnableCaching
@SpringBootApplication
public class BootBackendApplication {
	public static void main(String[] args) {
		SpringApplication.run(BootBackendApplication.class, args);
	}
}
```
---
### 在ProjectService中增加LOGGER
```
private static final Logger LOGGER = LoggerFactory.getLogger(ProjectService.class);
```
### 增加cache
```
    @Cacheable("projects")
    public Iterable<Project> findAllProjects() {
        LOGGER.info("get all projects");
        return projectRepository.findAll();
    }

```
---
### 增加條件
```java
    @Cacheable(value="projects",condition = "!#noCache")
    public Iterable<Project> findAllProjects(boolean noCache) {
        LOGGER.info("get all projects");
        return projectRepository.findAll();
    }

```
### 在project controller中使用
```java
    @GetMapping("/all")
    public Iterable<Project> getAllProjects() {
        return projectService.findAllProjects(true);
    }

```
---

# React frontend

## using create-react-app

```
npx create-react-app fluent_frontend
```

## add fluentUI

```
cd fluent_frontend
npm install @fluentui/react
```

---

## 在 src 下建立 components 目錄, 建立 DashBoard 的 rcc 元件

- 使用 Message 的元件

```
import React, { Component } from 'react'
import {MessageBar} from 'office-ui-fabric-react';
export default class Dashboard extends Component {
    render() {
        return (
            <div>
                <MessageBar>Welcome to dashboard</MessageBar>
            </div>
        )
    }
}

```

---

## App.js 中清空原本內容

- 加入 Dashboard
- 加入`import {loadTheme} from "@fluentui/react" ;`

```javascript
import React from "react";
import "./App.css";
import Dashboard from "./components/Dashboard";
import { loadTheme } from "@fluentui/react";
function App() {
  _loadTheme();
  return (
    <div className="App">
      <Dashboard />
    </div>
  );
}
```

---

## 使用字型

- 基本上從[https://developer.microsoft.com/en-us/fluentui#/styles/web/typography](https://developer.microsoft.com/en-us/fluentui#/styles/web/typography)

```javascript
// apply from https://developer.microsoft.com/en-us/fluentui#/styles/web/typography
function _loadTheme() {
  loadTheme({
    defaultFontStyle: {
      fontFamily: "Monaco, Menlo, Consolas",
      fontWeight: "regular",
    },
    fonts: {
      small: { fontSize: "22px" },
      medium: { fontSize: "26px" },
      large: {
        fontSize: "40px",
        fontWeight: "semibold",
      },
      xLarge: {
        fontSize: "44px",
        fontWeight: "semibold",
      },
    },
  });
}
export default App;
```

---

## 在`src\components`下新增`Project.js`

```javascript
import React, { Component } from "react";
import { Label } from "office-ui-fabric-react/lib/Label";
export default class Project extends Component {
  render() {
    return (
      <div>
        <Label>A Sample Project</Label>
      </div>
    );
  }
}
```

---

## 並且在`Dashboard`中使用

- 在 div 中, 在<MessageBar>的後面

```javascript
export default class Dashboard extends Component {
  render() {
    return (
      <div>
        <MessageBar>Welcome to dashboard</MessageBar>
        <Project />
        <Project />
        <Project />
      </div>
    );
  }
}
```

---

## 增加 Header 元件

- 使用 commandBar 元件

```javascript
import React, { Component } from "react";
import {
  CommandBar,
  ICommandBarItemProps,
} from "office-ui-fabric-react/lib/CommandBar";
export default class Header extends Component {
  render() {
    return (
      <div>
        <CommandBar items={_items} />
      </div>
    );
  }
}
const _items = [
  {
    key: "dashboard",
    text: "dashboard",
    iconProps: { iconName: "Home" },
    onClick: () => console.log("action1"),
  },
  {
    key: "other",
    text: "other",
    iconProps: { iconName: "Mail" },
    onClick: () => console.log("action2"),
  },
];
```

---

## 在 App.js 中初始化圖示, 並且加入 Header

```javascript
import React from "react";
import "./App.css";
import Dashboard from "./components/Dashboard";
import { loadTheme } from "@fluentui/react";
import Header from "./components/Header";
import { initializeIcons } from "office-ui-fabric-react/lib/Icons";
function App() {
  initializeIcons(); // initial icon first
  _loadTheme();
  return (
    <div className="App">
      <Header /> // add header
      <Dashboard />
    </div>
  );
}
```

---

# 增加專案的內容

---

## 修改 Project.js, 增加刪除和修改的內容

```jsx
import React, { Component } from "react";
import { Label } from "office-ui-fabric-react/lib/Label";
import "office-ui-fabric-react/dist/css/fabric.css";
import { PrimaryButton } from "office-ui-fabric-react";
export default class Project extends Component {
  render() {
    return (
      <div>
        <Label>A Sample Project</Label>
        <div className="ms-Grid" dir="ltr">
          <div className="ms-Grid-row">
            <Label>Board</Label>
          </div>
          <div className="ms-Grid-row">
            <div className="ms-Grid-col ms-sm4 ms-md4 ms-lg4">
              <PrimaryButton text="update/view" />
            </div>
            <div className="ms-Grid-col ms-sm4 ms-md4 ms-lg4">
              <PrimaryButton text="delete" />
            </div>
          </div>
        </div>
      </div>
    );
  }
}
```

---

## 增加 AddProject 新增的畫面

```jsx
import React, { Component } from 'react'
import { Label } from 'office-ui-fabric-react/lib/Label';
export default class AddProject extends Component {
    render() {
        return (
            <div>
                <Label>Add project form will be here</Label>
            </div>
        )
    }
}

```

## 新增 create-router-dom

```bash
cd fluented_frontend
npm install react-router-dom
```

---

## 修改 App.js

- 使用<BrowserRouter>包住元件裡面用<Route>

```jsx
import React from 'react';
import './App.css';
import Dashboard from './components/Dashboard';
import {loadTheme} from "@fluentui/react" ;
import Header from './components/Header';
import {initializeIcons} from 'office-ui-fabric-react/lib/Icons';
import { BrowserRouter, Route } from "react-router-dom";
import AddProject from './components/AddProject';
function App() {
  initializeIcons(); // initial icon first
  _loadTheme();
  return (
    <BrowserRouter>
    <div className="App">
      <Header/>
      <Route path="/dashboard" component={Dashboard}/>
      <Route path="/addproject" component={AddProject}/>
    </div>
    </BrowserRouter>
  );
}
```

---

## Header.js 中加入/dashboard 的連結

### 增加\_items 的`href`屬性

```jsx
const _items = [
  {
    key: "dashboard",
    text: "dashboard",
    iconProps: { iconName: "Home" },
    href: "/dashboard",
    onClick: () => console.log("action1"),
  },
  {
    key: "other",
    text: "other",
    iconProps: { iconName: "Mail" },
    onClick: () => console.log("action2"),
  },
];
```
---
## 在Dashboard.js中增加PrimaryButton
* 導向`/addProject`

```jsx
import React, { Component } from 'react'
import { MessageBar } from 'office-ui-fabric-react';
import Project from './Project';
import { PrimaryButton } from 'office-ui-fabric-react';
export default class Dashboard extends Component {
    render() {
        return (
            <div>
                <MessageBar>Welcome to dashboard</MessageBar>
                <PrimaryButton
                    text="Add Project"
                    onClick={() => window.location.href = "/addProject"} />
                <Project />
                <Project />
                <Project />
            </div>
        )
    }
}

```
---
## 進度
### 根目錄只有空白
### 點擊/dashboard會看到pseudo的列表
### 點擊Add Project按鈕會出現新增值
---
# 處理表單
---
## AddProject
---
## 使用redux
* 增加 redux相關的library
```shell
npm install redux react-redux redux-thunk axios
npm install redux-logger
npm install redux-devtools-extension
```
---
## 產生reducer
* 在reducer目錄下建立index.js, 加入下列 
```javascript
import { combineReducers } from "redux";
const rootReducer =  combineReducers({

})
export default rootReducer
```
---
## 在redux下建立store.js
* 創建redux-store
```
import { createStore, applyMiddleware, compose } from "redux";
import thunk from "redux-thunk";
import rootReducer from "../reducer";
import { composeWithDevTools } from 'redux-devtools-extension';
import logger from 'redux-logger';

const initialState = {}
const store = createStore(
  rootReducer,
  initialState,
  composeWithDevTools(applyMiddleware(logger, thunk))
)
export default store;

```
---
## 在App.js中使用redux
* 在App.js中引入store
```javascript
import { Provider } from "react-redux";
import store from "./redux/store";
```
* 把App的<div>包裝
```javascript
<Provider store={store}>
    <BrowserRouter>
    <div className="App">
        <Header />
        <Route path="/dashboard" component={Dashboard} />
        <Route path="/addproject" component={AddProject} />
    </div>
    </BrowserRouter>
</Provider>
```
---
## 定義ProjectTypes.js於redux/projects下
```javascript
export const POST_PROJECT_REQUEST = 'POST_PROJECT_REQUEST'
export const POST_PROJECT_SUCCESS = 'POST_PROJECT_SUCCESS'
export const POST_PROJECT_FAILURE = 'POST_PROJECT_FAILURE'
```
---
## 定義ProjectReducer於redux/projects下
```javascript
const { POST_PROJECT_REQUEST, POST_PROJECT_SUCCESS, POST_PROJECT_FAILURE } = require("./projectTypes")
const initialState = {
    loading: false,
    errors: ''
}
const projectReducer = (state = initialState, action) => {
    switch (action.type) {
        case POST_PROJECT_REQUEST:
            return {
                ...state,
                loading: true
            }
        case POST_PROJECT_SUCCESS:
            return {
                ...state,
                loading: false
            }
        case POST_PROJECT_FAILURE:
            return {
                ...state,
                loading: false,
                errors: action.payload
            }
        default: // this is important
            return state
    }
}
export default projectReducer
```
---
## 實作ProjectActions.js
```javascript
import axios from "axios";
import { POST_PROJECT_REQUEST, POST_PROJECT_SUCCESS, POST_PROJECT_FAILURE } 
from "./projectTypes";
const PROJECT_POST_URL = "http://localhost:8080/api/project"
export const postProjectRequest = () => {
    return { type: POST_PROJECT_REQUEST }
}
export const postProjectSuccess = () => {
    return { type: POST_PROJECT_SUCCESS }
}
export const postProjectFailure = (errors) => {
    return {
        type: POST_PROJECT_FAILURE,
        payload: errors
    }
}
```
---
## 實作ProjectActions.js (cont'd)
```javascript
// async action creator
export const createProject = (project, history) => dispatch => {
    dispatch(postProjectRequest())
    axios.post(PROJECT_POST_URL, project)
        .then(response => {
            dispatch(postProjectSuccess())
            history.push('/dashboard')
        }).catch(error => {
            const errorMessage = error.response.data
            dispatch(postProjectFailure(errorMessage))
            console.log(errorMessage)
        })
}
```
---
## 創建reducer下的index.js
```javascript
import { combineReducers } from "redux";
import projectReducer from "../redux/projects/projectReducer";
const rootReducer = combineReducers({
    project: projectReducer
})
export default rootReducer
```
---
## 創建stores.js
```javascript
import { createStore, applyMiddleware, compose } from "redux";
import thunk from "redux-thunk";
import rootReducer from "../reducer";
import { composeWithDevTools } from 'redux-devtools-extension';
import logger from 'redux-logger';

const initialState = {}
const store = createStore(
  rootReducer,
  composeWithDevTools(applyMiddleware(logger, thunk))
)
export default store;

```
---
## 回到AddProject.js
* 加入import
```javascript
import { connect } from "react-redux";
import { createProject } from "../redux/projects/projectActions";
```
* 將 AddProject連接到redux
```javascript
const mapStateToProps = state => {
    return {
        errors: state.project.errors
    }
}

export default connect(mapStateToProps, { createProject })(AddProject);
```
---
