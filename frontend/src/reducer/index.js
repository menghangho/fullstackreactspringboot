import { combineReducers } from "redux";
import errorReducer from "./errorReducer";
import projectReducer from "./projectReduer";
import backlogReducer from "./backReducer";
export default combineReducers({
  errors: errorReducer,
  project: projectReducer,
  backlog:backlogReducer
});
