package com.patristar.demo.Backend;

import com.patristar.demo.Backend.domain.Project;
import com.patristar.demo.Backend.repositories.ProjectRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(SpringExtension.class)
@DataJpaTest
public class DBTest {
    @Autowired
    private ProjectRepository repository;

    @Test
    void name() {
        Project p1 = new Project();
        p1.setProjectName("Mark Ho");
        p1.setProjectIdentifier("first_project1");
        p1.setDescription("this is my project");
        repository.save(p1);
        repository.save(p1);
        assertEquals(repository.count(),2);

    }
}
