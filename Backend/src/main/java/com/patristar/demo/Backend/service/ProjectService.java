package com.patristar.demo.Backend.service;

import com.patristar.demo.Backend.domain.Backlog;
import com.patristar.demo.Backend.domain.Project;
import com.patristar.demo.Backend.exceptions.ProjectIdException;
import com.patristar.demo.Backend.repositories.BacklogRepository;
import com.patristar.demo.Backend.repositories.ProjectRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProjectService {
    @Autowired
    private ProjectRepository projectRepository;
    @Autowired
    private BacklogRepository backlogRepository;
    public Project saveOrUpdateProject(Project project) {
        String projectIdentifier = project.getProjectIdentifier().toUpperCase();
        try {
            project.setProjectIdentifier(projectIdentifier);
            if(project.getId()==null) {
                Backlog backlog = new Backlog();
                project.setBacklog(backlog);
                backlog.setProject(project);
                backlog.setProjectIdentifier(projectIdentifier);

            }
            if (project.getId()!=null) {
                project.setBacklog(backlogRepository.findByProjectIdentifier(projectIdentifier));
            }
            return projectRepository.save(project);
        }catch (Exception e){
            throw new ProjectIdException("Project ID"+ projectIdentifier +" already exists");
        }
    }
    public Project findProjectByIdentifier(String projectId) {
        Project project = projectRepository.findByProjectIdentifier(projectId.toUpperCase());
        if (project == null) {
            throw new ProjectIdException("Project ID"+projectId+" does not exist");
        }
        return project;
    }
    public Iterable<Project> findAllProjects() {
        return projectRepository.findAll();
    }
    public void deleteProjectByIdentifier(String projectId) {
        // hand writing
        Project project = projectRepository.findByProjectIdentifier(projectId.toUpperCase());

        if (project == null) {
            throw new ProjectIdException("Cannot delete project with id: '"+projectId+"', this project does not exist");

        }
        projectRepository.delete(project);

    }
}
