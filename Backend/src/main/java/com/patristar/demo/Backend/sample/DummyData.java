package com.patristar.demo.Backend.sample;

import com.patristar.demo.Backend.domain.Project;
import com.patristar.demo.Backend.service.ProjectService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class DummyData implements CommandLineRunner {
    @Autowired
    private ProjectService service;
    private static final Logger LOGGER = LoggerFactory.getLogger(DummyData.class.getSimpleName());
    @Override
    public void run(String... args)  {
        LOGGER.info("add sample data");
        Project project = new Project();
        project.setProjectName("Dummy project1");
        project.setProjectIdentifier("DUMMY_1");
        project.setDescription("this is a general project by spring boot 1");
        service.saveOrUpdateProject(project);
    }
}
