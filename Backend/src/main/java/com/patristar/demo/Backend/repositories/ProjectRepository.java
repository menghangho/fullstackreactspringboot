package com.patristar.demo.Backend.repositories;

import com.patristar.demo.Backend.domain.Project;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProjectRepository extends CrudRepository<Project, Long> {
    Project findByProjectIdentifier (String projectId);
}
