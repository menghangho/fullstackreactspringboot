package com.patristar.demo.Backend.service;

import com.patristar.demo.Backend.domain.Backlog;
import com.patristar.demo.Backend.domain.Project;
import com.patristar.demo.Backend.domain.ProjectTask;
import com.patristar.demo.Backend.exceptions.ProjectNotFoundException;
import com.patristar.demo.Backend.repositories.BacklogRepository;
import com.patristar.demo.Backend.repositories.ProjectRepository;
import com.patristar.demo.Backend.repositories.ProjectTaskRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.validation.constraints.Null;
import java.util.List;

@Service
public class ProjectTasksService {
    @Autowired
    private BacklogRepository backlogRepository;
    @Autowired
    private ProjectTaskRepository projectTaskRepository;
    @Autowired
    private ProjectRepository projectRepository;

    public ProjectTask addProjectTask(String projectIdentifier, ProjectTask projectTask) {
        Backlog backlog = backlogRepository.findByProjectIdentifier(projectIdentifier);

        if (backlog == null) {
            throw new ProjectNotFoundException("project not found by id");
        }


        projectTask.setBacklog(backlog);
        Integer backlogSequence = backlog.getPTSequence();
        backlogSequence++;
        backlog.setPTSequence(backlogSequence);
        projectTask.setProjectSequence(projectIdentifier + "-" + backlogSequence);
        projectTask.setProjectIdentifier(projectIdentifier);
        if (projectTask.getPriority() == null || projectTask.getPriority() == 0) {
            projectTask.setPriority(3);
        }
        if (projectTask.getStatus() == "" || projectTask.getStatus() == null) {
            projectTask.setStatus("TO_DO");
        }
        return projectTaskRepository.save(projectTask);
    }

    public Iterable<ProjectTask> findBacklogById(String backlog_id) {
        Project project = projectRepository.findByProjectIdentifier(backlog_id);
        if (project == null) {
            throw new ProjectNotFoundException("Project with id: "+backlog_id+" does not exist");
        }

        return projectTaskRepository.findByProjectIdentifierOrderByPriority(backlog_id);
    }
    public ProjectTask findPTByProjectSequence(String backlog_id, String pt_id) {

        Backlog backlog = backlogRepository.findByProjectIdentifier(backlog_id);
        if (backlog == null) {
            throw new ProjectNotFoundException("Project with ID:"+backlog_id+" does not exist");
        }
        ProjectTask task = projectTaskRepository.findByProjectSequence(pt_id);
        if (task==null) {
            throw new ProjectNotFoundException("Project task with ID:"+pt_id+" does not exist ");
        }

        if (!task.getProjectIdentifier().equals(backlog_id)) {
            throw new ProjectNotFoundException("Project task "+pt_id+" does not exist in project:"+backlog_id);
        }
        return task;
    }
    public ProjectTask updateByProjectSequence(ProjectTask updateTask, String backlog_id, String pt_id){
//        ProjectTask task = projectTaskRepository.findByProjectSequence(updateTask.getProjectSequence());
//        task = updateTask;
        return projectTaskRepository.save(updateTask);
    }
    public void deletePTByProjectSequence( String backlog_id, String pt_id) {
        ProjectTask task = findPTByProjectSequence(backlog_id, pt_id);
//        Backlog backlog = task.getBacklog();
//        List<ProjectTask> tasks = backlog.getProjectTasks();
//        tasks.remove(task);
//        backlogRepository.save(backlog);
        projectTaskRepository.delete(task);
    }
}
